# import libraries to help read and create PDF
import base64
import json

# import the Elasticsearch low-level client library
from elasticsearch import Elasticsearch

import requests

INDEX_NAME = "pdf2"
MAPPING = "mapping2.json"

# create a new client instance of Elasticsearch
elastic_client = Elasticsearch(hosts=["localhost"])



def send_doc(file):

	f = open(file, "rb")
	data = f.read()
	f.close()

	encoded_doc = str(base64.b64encode(data).decode("utf-8")) # do not forget to remove b''

	response = requests.post('http://localhost:9200/' + INDEX_NAME + '/_doc', 
			params = {'pipeline':'attachment'},
			json =   { "data": encoded_doc })

	print("result:",response.url,response.text,"\n")

def main():

	from os import listdir
	from os.path import isfile, join

	#mypath = "/Users/yvesbenigot/Documents/datasets/NLP/CUAD_v1/full_contract_pdf/Part_I/IP/"
	mypath = "/Users/yvesbenigot/Documents/datasets/NLP/CUAD_v1/full_contract_pdf/test/"
	#mypath = "/Users/yvesbenigot/Documents/gin-projects/elasticdoc/test/"
	onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f)) and f.endswith('.pdf')]

	print("using mapping file ", MAPPING)
	with open(MAPPING) as json_file:
		index_body = json.load(json_file)
		try :
			elastic_client.indices.delete(index=INDEX_NAME)
		except Exception:
			print("cannot delete index")
		try :
			elastic_client.indices.create(index=INDEX_NAME, body=index_body)
		except Exception:
			print("cannot create index")

	for f in onlyfiles:
		print("doc:", f)
		send_doc(mypath + f)


if __name__ == "__main__":
	# execute only if run as a script
	main()



