

# import libraries to help read and create PDF
import base64
import json
import os

# import the Elasticsearch low-level client library
from elasticsearch import Elasticsearch

import requests
import sys

import pdfminer.high_level as h

'''from flair.data import Sentence
from flair.models import SequenceTagger
from flair.embeddings import WordEmbeddings, DocumentPoolEmbeddings

print("imports done")
glove_embedding = WordEmbeddings('glove')
document_embeddings = DocumentPoolEmbeddings([glove_embedding])# return embeddings for doc
print("embeddings done")

def embed_text(doc): 
	print("doc:",doc)
	sentence = Sentence(doc)
	document_embeddings.embed(sentence)
	embeddings = sentence.embedding.numpy() 
	print("embeddings:",embeddings)
	return embeddings
'''
import spacy

# Load the spacy model that you have installed
nlp = spacy.load('en_core_web_lg') # md


def l1_norm(v):
	return sum([abs(x) for x in v.tolist()])

def embed_text(doc_text):
	doc = nlp(doc_text)
	vector = doc.vector
	if l1_norm(vector) == 0: # never embed as zero vector as this computes to NaN in cosine similarity
		vector[0] = 1.0e-12
		print("zero vector timed!")
	return vector

INDEX_NAME = "pdfembedded"
MAPPING = "mapping_vector.json"


def send_doc(file):

	print("process file:",file)

	with open(file, "rb") as f:
		data = f.read()
		encoded_doc = str(base64.b64encode(data).decode("utf-8")) # do not forget to remove b''
		f.close()

	doc_text = h.extract_text(file) # use pdfminer.six as PY2PDF does not work on all docs
	doc_vector = embed_text(doc_text) # size 100

	print("doc text length",len(doc_text),"doc vector len", len(doc_vector.tolist()))

	response = requests.post('http://localhost:9200/' + INDEX_NAME + '/_doc', 
			params = {'pipeline':'attachment'},
			json =   { "data": encoded_doc, "text_dense_vector" : doc_vector.tolist(), "status" : "published" })

	print("result:",response.url,response.text,"\n")

def main():

	from os import listdir
	from os.path import isfile, join

	mypath = "/Users/yvesbenigot/Documents/datasets/NLP/CUAD_v1/full_contract_pdf/"

	# create a new client instance of Elasticsearch
	elastic_client = Elasticsearch(hosts=["localhost"])

	print("using mapping file ", MAPPING)
	with open(MAPPING) as json_file:
		index_body = json.load(json_file)
		try :
			elastic_client.indices.delete(index=INDEX_NAME)
		except Exception:
			print("cannot delete index")
		try :
			elastic_client.indices.create(index=INDEX_NAME, body=index_body)
		except Exception:
			print("cannot create index")

	nb_files = 0
	for path, subdirs, files in os.walk(mypath):
		for name in files:
			if name.endswith('.pdf'):
				print("directory:",path, "subdirs;", subdirs, "\nfile:", name)
				nb_files += 1
				send_doc(path + '/' + '/'.join(subdirs) + name)
	print("nb files:", nb_files)

if __name__ == "__main__":
	# execute only if run as a script
	main()



