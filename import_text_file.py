# import libraries to help read and create PDF
import json

# import the Elasticsearch low-level client library
from elasticsearch import Elasticsearch

count=0


def send_doc(text):

	global count

	# create a new client instance of Elasticsearch
	elastic_client = Elasticsearch(hosts=["localhost"])

	# put the PDF data into a dictionary body to pass to the API request
	body_doc = {"data": text}

	# call the index() method to index the data
	count += 1
	result = elastic_client.index(index="text", doc_type="_doc", id=str(count), body=body_doc)

	# print the returned results
	print ("\nindex result:", result['result'])

def process_docs(file):
	f = open(file, "r")
	text = f.read()
	print(text) 
	send_doc(text)
	f.close()

def main():

	from os import listdir
	from os.path import isfile, join

	mypath = "/Users/yvesbenigot/Documents/datasets/NLP/CUAD_v1/full_contract_txt/"
	onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

	for f in onlyfiles:
		process_docs(mypath + f)


if __name__ == "__main__":
    # execute only if run as a script
    main()



