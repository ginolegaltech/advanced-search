from flair.data import Sentence
from flair.models import SequenceTagger
from flair.embeddings import WordEmbeddings, DocumentPoolEmbeddings

import sys
import requests
from datetime import datetime 

import spacy

# Load the spacy model that you have installed
nlp = spacy.load('en_core_web_lg') # md


def l1_norm(v):
	return sum([abs(x) for x in v.tolist()])

def embed_text(doc_text):
	doc = nlp(doc_text)
	vector = doc.vector
	if l1_norm(vector) == 0: # never embed as zero vector as this computes to NaN in cosine similarity
		vector[0] = 1.0e-12
		print("zero vector timed!")
	return vector

def print_answer(a):
	print("----------------------\ntook:",a['took'])
	count=0
	for h in a['hits']['hits']:
		print("hit index",h['_index'],"type:",h['_type'],"id:",h['_id'],"score:",h['_score'])
		count+=1
	print(count,"hits")
	print("----------------------\n")
	
def main(query):
	INDEX_NAME="pdfembedded"
	SEARCH_SIZE=10

	query_vector = embed_text(query) # size 100
	
	script_query = {
			"script_score": {
				"query": {"match_all": {}},
				"script": {
					"source": "cosineSimilarity(params.query_vector, 'text_dense_vector') + 1.0",
					"params": {"query_vector": query_vector.tolist()} # fixed_test_vector} #
					}
				}
			}
	body = {
			"size": SEARCH_SIZE,
			"query": script_query
		}
	
	start_time = datetime.now() 

	response = requests.post('http://localhost:9200/' + INDEX_NAME + '/_search?pretty=true', json = body)

	time_elapsed = datetime.now() - start_time 
	print('Time elapsed (hh:mm:ss.ms) {}'.format(time_elapsed))

	print_answer(response.json())
	print("response:", response)
	with open("result.json","w") as f:
		f.write(response.text)


if __name__ == "__main__":
	query = sys.argv[1] # test franchise vs prerogative
	main(query)